## REST API for Team tasks list 

### Requirements
* Java 11
* Maven

### Running the project

The app will run on http://localhost:8080/
```shell script
$ git clone https://bitbucket.org/mariapoprawa/teamtasks.git
$ cd teamtasks
$ ./mvnw install
$ ./mvnw spring-boot:run
```

### API documentation

To see the docs, visit http://localhost:8080/swagger-ui.html