package com.example.teamtasks.repository;

import com.example.teamtasks.model.Status;
import com.example.teamtasks.model.Task;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TaskRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void shouldSaveValidTask() {
        var task = getValidTask();

        taskRepository.save(task);
        entityManager.flush();

        assertNotNull(task.getId());
        assertEquals("write some tests", task.getTitle());
        assertEquals(Status.IN_PROGRESS, task.getStatus());
        assertEquals(5, task.getSize());
        assertNull(task.getAssignee());
    }

    @ParameterizedTest
    @MethodSource("getInvalidTasks")
    public void shouldThrowConstraintViolationExceptionWhenSavingInvalidTask(Task task) {
        assertThrows(ConstraintViolationException.class, () -> {
            taskRepository.save(task);
            entityManager.flush();
        });
    }

    private static Task getValidTask() {
        return new Task("write some tests", Status.IN_PROGRESS, 5);
    }

    private static List<Task> getInvalidTasks() {
        var nullTitleTask = getValidTask();
        nullTitleTask.setTitle(null);

        var emptyTitleTask = getValidTask();
        emptyTitleTask.setTitle("");

        var blankTitleTask = getValidTask();
        blankTitleTask.setTitle("   ");

        var nullStatusTask = getValidTask();
        nullStatusTask.setStatus(null);

        var nonFibonacciSizeTask = getValidTask();
        nonFibonacciSizeTask.setSize(4);

        return List.of(nullTitleTask, emptyTitleTask, blankTitleTask, nullStatusTask, nonFibonacciSizeTask);
    }
}
