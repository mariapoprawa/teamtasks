package com.example.teamtasks.repository;

import com.example.teamtasks.model.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PersonRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void shouldSaveValidPerson() {
        var person = new Person("Maria", 30);

        personRepository.save(person);
        entityManager.flush();

        assertNotNull(person.getId());
        assertEquals("Maria", person.getName());
        assertEquals(30, person.getCapacity());
        assertNotNull(person.getTasks());
        assertTrue(person.getTasks().isEmpty());
    }

    @ParameterizedTest
    @MethodSource("getInvalidPersons")
    public void shouldThrowConstraintViolationExceptionWhenSavingInvalidPerson(Person person) {
        assertThrows(ConstraintViolationException.class, () -> {
            personRepository.save(person);
            entityManager.flush();
        });
    }

    private static List<Person> getInvalidPersons() {
        var nullNamePerson = new Person(null, 30);
        var emptyNamePerson = new Person("", 30);
        var blankNamePerson = new Person("   ", 30);
        var negativeCapacityPerson = new Person("Maria", -1);
        var tooLargeCapacityPerson = new Person("Maria", 41);

        return List.of(nullNamePerson, emptyNamePerson, blankNamePerson, negativeCapacityPerson, tooLargeCapacityPerson);
    }
}
