package com.example.teamtasks.repository.specification;

import com.example.teamtasks.model.Status;
import com.example.teamtasks.model.Task;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.criteria.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TaskSpecificationTest {

    @Mock
    private Root<Task> root;

    @Mock
    private Join<Object, Object> personJoin;

    @Mock
    private Path<Object> personIdPath;

    @Mock
    private Path<Object> statusPath;

    @Mock
    private CriteriaQuery<?> criteriaQuery;

    @Mock
    private CriteriaBuilder criteriaBuilder;

    @Mock
    private Predicate predicate;

    @BeforeEach
    public void setup() {
        when(root.join("assignee", JoinType.LEFT)).thenReturn(personJoin);
        when(personJoin.get("id")).thenReturn(personIdPath);
    }

    @Test
    public void shouldNotFilterTasks() {
        var spec = new TaskSpecification(null, null, null);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verifyNoInteractions(criteriaBuilder);
    }

    @Test
    public void shouldFilterByStatus() {
        var spec = new TaskSpecification(Status.OPEN, null, null);

        when(root.get("status")).thenReturn(statusPath);
        when(criteriaBuilder.equal(statusPath, Status.OPEN)).thenReturn(predicate);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verify(criteriaBuilder).equal(statusPath, Status.OPEN);
        verifyNoMoreInteractions(criteriaBuilder);
    }

    @Test
    public void shouldFilterByAssignedTrue() {
        var spec = new TaskSpecification(null, null, true);

        when(criteriaBuilder.isNotNull(personIdPath)).thenReturn(predicate);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verify(criteriaBuilder).isNotNull(personIdPath);
        verifyNoMoreInteractions(criteriaBuilder);
    }

    @Test
    public void shouldFilterByAssignedFalse() {
        var spec = new TaskSpecification(null, null, false);

        when(criteriaBuilder.isNull(personIdPath)).thenReturn(predicate);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verify(criteriaBuilder).isNull(personIdPath);
        verifyNoMoreInteractions(criteriaBuilder);
    }

    @Test
    public void shouldFilterByAssignee() {
        var spec = new TaskSpecification(null, 1L, null);

        when(criteriaBuilder.equal(personIdPath, 1L)).thenReturn(predicate);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verify(criteriaBuilder).equal(personIdPath, 1L);
        verifyNoMoreInteractions(criteriaBuilder);
    }

    @Test
    public void shouldFilterByStatusAndAssignee() {
        var spec = new TaskSpecification(Status.CLOSED, 1L, null);

        when(root.get("status")).thenReturn(statusPath);
        when(criteriaBuilder.equal(statusPath, Status.CLOSED)).thenReturn(predicate);
        when(criteriaBuilder.equal(personIdPath, 1L)).thenReturn(predicate);
        when(criteriaBuilder.and(any(), any())).thenReturn(predicate);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verify(criteriaBuilder).equal(statusPath, Status.CLOSED);
        verify(criteriaBuilder).equal(personIdPath, 1L);
        verify(criteriaBuilder).and(any(), any());
        verifyNoMoreInteractions(criteriaBuilder);
    }

    @Test
    public void shouldFilterByStatusAndAssignedTrue() {
        var spec = new TaskSpecification(Status.CLOSED, null, true);

        when(root.get("status")).thenReturn(statusPath);
        when(criteriaBuilder.equal(statusPath, Status.CLOSED)).thenReturn(predicate);
        when(criteriaBuilder.isNotNull(personIdPath)).thenReturn(predicate);
        when(criteriaBuilder.and(any(), any())).thenReturn(predicate);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verify(criteriaBuilder).equal(statusPath, Status.CLOSED);
        verify(criteriaBuilder).isNotNull(personIdPath);
        verify(criteriaBuilder).and(any(), any());
        verifyNoMoreInteractions(criteriaBuilder);
    }

    @Test
    public void shouldFilterByStatusAndAssignedFalse() {
        var spec = new TaskSpecification(Status.IN_PROGRESS, null, false);

        when(root.get("status")).thenReturn(statusPath);
        when(criteriaBuilder.equal(statusPath, Status.IN_PROGRESS)).thenReturn(predicate);
        when(criteriaBuilder.isNull(personIdPath)).thenReturn(predicate);
        when(criteriaBuilder.and(any(), any())).thenReturn(predicate);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verify(criteriaBuilder).equal(statusPath, Status.IN_PROGRESS);
        verify(criteriaBuilder).isNull(personIdPath);
        verify(criteriaBuilder).and(any(), any());
        verifyNoMoreInteractions(criteriaBuilder);
    }

    @Test
    public void shouldIgnoreAssignedIfGivenAssignee() {
        var spec = new TaskSpecification(null, 2L, false);

        when(criteriaBuilder.equal(personIdPath, 2L)).thenReturn(predicate);

        spec.toPredicate(root, criteriaQuery, criteriaBuilder);

        verify(criteriaBuilder).equal(personIdPath, 2L);
        verifyNoMoreInteractions(criteriaBuilder);
    }
}
