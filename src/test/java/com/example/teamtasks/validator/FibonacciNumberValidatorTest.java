package com.example.teamtasks.validator;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FibonacciNumberValidatorTest {

    private FibonacciNumberValidator validator = new FibonacciNumberValidator();

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 8, 21})
    public void shouldValidateFibonacciNumber(int number) {
        assertTrue(validator.isValid(number, null));
    }

    @ParameterizedTest
    @ValueSource(ints = {-5, -2, 4, 9, 20})
    public void shouldNotValidateNonFibonacciNumber(int number) {
        assertFalse(validator.isValid(number, null));
    }

}
