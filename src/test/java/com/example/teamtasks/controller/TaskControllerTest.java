package com.example.teamtasks.controller;

import com.example.teamtasks.dto.TaskDto;
import com.example.teamtasks.exception.ResourceNotFoundException;
import com.example.teamtasks.model.Status;
import com.example.teamtasks.model.Task;
import com.example.teamtasks.service.TaskService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TaskController.class)
public class TaskControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaskService taskService;

    @Test
    public void shouldCreateTask() throws Exception {
        mockMvc.perform(post("/tasks")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"title\": \"Do it\", \"status\": \"OPEN\", \"size\": 3}"))
                .andExpect(status().isOk());

        ArgumentCaptor<TaskDto> task = forClass(TaskDto.class);
        verify(taskService).createTask(task.capture());

        assertEquals("Do it", task.getValue().getTitle());
        assertEquals(Status.OPEN, task.getValue().getStatus());
        assertEquals(3, task.getValue().getSize());
    }

    @Test
    public void shouldReturnTaskIfExists() throws Exception {
        var task = new Task("Do something", Status.IN_PROGRESS, 5);
        when(taskService.getTask(1L)).thenReturn(task);

        mockMvc.perform(get("/tasks/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"title\": \"Do something\", \"status\": \"IN_PROGRESS\", \"size\": 5}"));
    }

    @Test
    public void shouldReturnNotFoundIfTaskDoesNotExist() throws Exception {
        when(taskService.getTask(2L)).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(get("/tasks/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldUpdateTaskIfExists() throws Exception {
        mockMvc.perform(put("/tasks/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"title\": \"Do anything\", \"status\": \"CLOSED\", \"size\": 1}"))
                .andExpect(status().isNoContent());

        ArgumentCaptor<TaskDto> task = forClass(TaskDto.class);
        verify(taskService).updateTask(eq(1L), task.capture());

        assertEquals("Do anything", task.getValue().getTitle());
        assertEquals(Status.CLOSED, task.getValue().getStatus());
        assertEquals(1, task.getValue().getSize());
    }

    @Test
    public void shouldReturnNotFoundIfUpdatingTaskDoesNotExist() throws Exception {
        doThrow(new ResourceNotFoundException()).when(taskService).updateTask(eq(2L), any());

        mockMvc.perform(put("/tasks/2")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"title\": \"Do anything\", \"status\": \"CLOSED\", \"size\": 1}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldDeleteTaskIfExists() throws Exception {
        mockMvc.perform(delete("/tasks/1"))
                .andExpect(status().isNoContent());

        verify(taskService).deleteTask(1L);
    }

    @Test
    public void shouldReturnNotFoundIfRemovingTaskDoesNotExist() throws Exception {
        doThrow(new ResourceNotFoundException()).when(taskService).deleteTask(2L);

        mockMvc.perform(delete("/tasks/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnAllTasks() throws Exception {
        var task1 = new Task("T1", Status.OPEN, 1);
        var task2 = new Task("T2", Status.CLOSED, 2);
        when(taskService.getTasks(null, null, null)).thenReturn(List.of(task1, task2));

        mockMvc.perform(get("/tasks"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"title\": \"T1\", \"status\": \"OPEN\", \"size\": 1},"
                        + "{\"title\": \"T2\", \"status\": \"CLOSED\", \"size\": 2}]"));
    }

    @Test
    public void shouldReturnFilteredTasks() throws Exception {
        var task1 = new Task("Important story", Status.OPEN, 8);
        when(taskService.getTasks(Status.OPEN, null, false)).thenReturn(List.of(task1));

        mockMvc.perform(get("/tasks?status=OPEN&assigned=0"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"title\": \"Important story\", \"status\": \"OPEN\", \"size\": 8}]"));
    }

    @Test
    public void shouldReassignOpenTasks() throws Exception {
        mockMvc.perform(post("/tasks/reassign"))
                .andExpect(status().isNoContent());

        verify(taskService).reassignOpenTasks();
    }
}
