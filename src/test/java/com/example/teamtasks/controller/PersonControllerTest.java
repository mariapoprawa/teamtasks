package com.example.teamtasks.controller;

import com.example.teamtasks.dto.PersonDto;
import com.example.teamtasks.exception.ResourceNotFoundException;
import com.example.teamtasks.model.Person;
import com.example.teamtasks.service.PersonService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PersonController.class)
public class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonService personService;

    @Test
    public void shouldCreatePerson() throws Exception {
        mockMvc.perform(post("/persons")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"name\": \"Maria\", \"capacity\": 20}"))
                .andExpect(status().isOk());

        ArgumentCaptor<PersonDto> person = forClass(PersonDto.class);
        verify(personService).createNewPerson(person.capture());

        assertEquals("Maria", person.getValue().getName());
        assertEquals(20, person.getValue().getCapacity());
    }

    @Test
    public void shouldReturnPersonIfExists() throws Exception {
        var person = new Person("Test", 10);
        when(personService.getPerson(1L)).thenReturn(person);

        mockMvc.perform(get("/persons/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"name\":\"Test\",\"capacity\":10}"));
    }

    @Test
    public void shouldReturnNotFoundIfPersonDoesNotExist() throws Exception {
        when(personService.getPerson(2L)).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(get("/persons/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldDeletePersonIfExists() throws Exception {
        mockMvc.perform(delete("/persons/1"))
                .andExpect(status().isNoContent());

        verify(personService).deletePerson(1L);
    }

    @Test
    public void shouldReturnNotFoundIfRemovingPersonDoesNotExist() throws Exception {
        doThrow(new ResourceNotFoundException()).when(personService).deletePerson(2L);

        mockMvc.perform(delete("/persons/2"))
                .andExpect(status().isNotFound());
    }
}
