package com.example.teamtasks.service;

import com.example.teamtasks.dto.PersonDto;
import com.example.teamtasks.exception.NotEnoughCapacityLeftException;
import com.example.teamtasks.exception.ResourceNotFoundException;
import com.example.teamtasks.model.Person;
import com.example.teamtasks.model.Status;
import com.example.teamtasks.model.Task;
import com.example.teamtasks.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private PersonService personService;

    private PersonDto personDto = new PersonDto("Jack", 20);
    private Person person = new Person("Jack", 20);
    private Long personId = 4L;

    @Test
    public void shouldSavePersonInRepository() {
        personService.createNewPerson(personDto);

        verify(personRepository).save(person);
    }

    @Test
    public void shouldDeletePersonIfFoundInRepository() {
        when(personRepository.findById(personId)).thenReturn(Optional.of(person));

        personService.deletePerson(personId);

        verify(personRepository).delete(person);
    }

    @Test
    public void shouldThrowResourceNotFoundExceptionIfDeletingPersonNotFoundInRepository() {
        when(personRepository.findById(personId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> personService.deletePerson(personId));

        verify(personRepository, never()).delete(any());
    }

    @Test
    public void shouldGetPersonIfFoundInRepository() {
        when(personRepository.findById(personId)).thenReturn(Optional.of(person));

        var result = personService.getPerson(personId);

        assertEquals(person, result);
    }

    @Test
    public void shouldThrowResourceNotFoundExceptionIfPersonNotFoundInRepository() {
        when(personRepository.findById(personId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> personService.getPerson(personId));
    }

    @Test
    public void shouldCalculateCapacityLeft() {
        var testPerson = new Person("Test", 35);
        testPerson.getTasks().addAll(List.of(new Task("Add integration tests", Status.OPEN, 2),
                new Task("Add endpoint for adding tasks", Status.IN_PROGRESS, 3),
                new Task("Add endpoint for updating tasks", Status.CLOSED, 8)));

        var result = personService.calculateCapacityLeft(testPerson);

        assertEquals(22, result);
    }

    @Test
    public void shouldFindPersonWithTheLowestCapacityThatIsEnoughForGivenTaskSize() {
        var person1 = new Person("John", 15);
        person1.getTasks().add(new Task("Do something", Status.OPEN, 5));

        var person2 = new Person("Tom", 15);
        person2.getTasks().add(new Task("Write docs", Status.OPEN, 8));

        var person3 = new Person("Alex", 15);
        person3.getTasks().add(new Task("Remove unused code", Status.OPEN, 13));

        when(personRepository.findAll()).thenReturn(List.of(person1, person2, person3));

        var result = personService.findPersonWithCapacityLeft(5);

        assertTrue(result.isPresent());
        assertEquals(person2, result.get());
    }

    @Test
    public void shouldAssignTaskAndSavePersonIfHasCapacityLeft() {
        var person = new Person("Test", 10);
        var task = new Task("Do anything", Status.OPEN, 3);

        personService.assignTask(person, task);

        assertEquals(1, person.getTasks().size());
        assertTrue(person.getTasks().contains(task));
        verify(personRepository).save(person);
    }

    @Test
    public void shouldThrowNotEnoughCapacityLeftExceptionIfPersonDoesNotHaveCapacityLeft() {
        var person = new Person("Test", 10);
        person.getTasks().add(new Task("Exercise", Status.IN_PROGRESS, 3));

        var task = new Task("Do anything", Status.OPEN, 8);

        assertThrows(NotEnoughCapacityLeftException.class, () -> {
            personService.assignTask(person, task);
        });

        assertEquals(1, person.getTasks().size());
        assertFalse(person.getTasks().contains(task));
        verify(personRepository, never()).save(person);
    }
}
