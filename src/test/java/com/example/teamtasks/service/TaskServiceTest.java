package com.example.teamtasks.service;

import com.example.teamtasks.dto.TaskDto;
import com.example.teamtasks.exception.ResourceNotFoundException;
import com.example.teamtasks.model.Person;
import com.example.teamtasks.model.Status;
import com.example.teamtasks.model.Task;
import com.example.teamtasks.repository.TaskRepository;
import com.example.teamtasks.repository.specification.TaskSpecification;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TaskServiceTest {

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private PersonService personService;

    @InjectMocks
    private TaskService taskService;

    private Task task = new Task("write some tests", Status.IN_PROGRESS, 3);
    private TaskDto taskDto = new TaskDto("write some tests", Status.IN_PROGRESS, 3, null);
    private Long taskId = 3L;
    private TaskSpecification emptyTaskSpecification = new TaskSpecification(null, null, null);
    private Person assignee = new Person("Jack", 30);

    @Test
    public void shouldSaveTaskInRepositoryAndAssignIt() {
        var taskWithAssignee = new Task("write some tests", Status.IN_PROGRESS, 3);
        taskWithAssignee.setAssignee(assignee);

        when(personService.findPersonWithCapacityLeft(3)).thenReturn(Optional.of(assignee));

        taskService.createTask(taskDto);

        verify(taskRepository).save(taskWithAssignee);
    }

    @Test
    public void shouldSaveTaskInRepositoryWithoutAssignee() {
        when(personService.findPersonWithCapacityLeft(3)).thenReturn(Optional.empty());

        taskService.createTask(taskDto);

        verify(taskRepository).save(task);
        assertNull(task.getAssignee());
    }

    @Test
    public void shouldGetTaskIfFoundInRepository() {
        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));

        var result = taskService.getTask(taskId);

        assertEquals(task, result);
    }

    @Test
    public void shouldThrowResourceNotFoundExceptionIfTaskNotFoundInRepository() {
        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> taskService.getTask(taskId));
    }

    @Test
    public void shouldThrowResourceNotFoundExceptionIfUpdatingTaskNotFoundInRepository() {
        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> taskService.updateTask(taskId, taskDto));
    }

    @Test
    public void shouldDeleteTaskIfFoundInRepository() {
        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));

        taskService.deleteTask(taskId);

        verify(taskRepository).delete(task);
    }

    @Test
    public void shouldThrowResourceNotFoundExceptionIfDeletingTaskNotFoundInRepository() {
        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> taskService.deleteTask(taskId));

        verify(taskRepository, never()).delete(any());
    }

    @Test
    public void shouldReturnAllTasks() {
        var task2 = new Task("write more tests", Status.OPEN, 2);

        when(taskRepository.findAll(emptyTaskSpecification)).thenReturn(List.of(task, task2));

        var result = taskService.getTasks(null, null, null);

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void shouldReturnEmptyListIfNoTasks() {
        when(taskRepository.findAll(emptyTaskSpecification)).thenReturn(emptyList());

        var result = taskService.getTasks(null, null, null);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }


    @Test
    public void shouldFilterTasks() {
        var specification = new TaskSpecification(Status.IN_PROGRESS, 1L, true);

        when(taskRepository.findAll(specification)).thenReturn(singletonList(task));

        var result = taskService.getTasks(Status.IN_PROGRESS, 1L, true);

        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void shouldReassignOpenTasksIfPersonWithEnoughCapacityFound() {
        var task1 = new Task("write some tests", Status.OPEN, 5);
        var task2 = new Task("write some tests", Status.OPEN, 2);

        when(taskRepository.findAllByAssigneeIsNullOrderBySizeDesc()).thenReturn(List.of(task1, task2));
        when(personService.findPersonWithCapacityLeft(5)).thenReturn(Optional.empty());
        when(personService.findPersonWithCapacityLeft(2)).thenReturn(Optional.of(assignee));

        taskService.reassignOpenTasks();

        verify(personService, never()).assignTask(any(), eq(task1));
        verify(personService).assignTask(assignee, task2);

        assertNull(task1.getAssignee());
        assertEquals(assignee, task2.getAssignee());
    }
}
