package com.example.teamtasks.dto;

import com.example.teamtasks.model.Status;
import com.example.teamtasks.validator.FibonacciNumber;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TaskDto {
    @NotBlank
    private String title;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @FibonacciNumber
    private int size;

    private Long assigneeId;

    public TaskDto(@NotBlank String title, @NotNull Status status, int size, Long assigneeId) {
        this.title = title;
        this.status = status;
        this.size = size;
        this.assigneeId = assigneeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Long getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Long assigneeId) {
        this.assigneeId = assigneeId;
    }
}
