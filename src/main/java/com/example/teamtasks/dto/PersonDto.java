package com.example.teamtasks.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class PersonDto {
    @NotBlank
    private String name;

    @Min(0)
    @Max(40)
    private int capacity;

    public PersonDto(@NotBlank String name, @Min(0) @Max(40) int capacity) {
        this.name = name;
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
