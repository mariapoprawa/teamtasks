package com.example.teamtasks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamtasksApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeamtasksApplication.class, args);
	}

}
