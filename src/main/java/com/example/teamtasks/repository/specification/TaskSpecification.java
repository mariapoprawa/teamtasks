package com.example.teamtasks.repository.specification;

import com.example.teamtasks.model.Status;
import com.example.teamtasks.model.Task;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Objects;
import java.util.stream.Stream;

public class TaskSpecification implements Specification<Task> {

    private final Status status;

    private final Long assigneeId;

    private final Boolean assigned;

    public TaskSpecification(Status status, Long assigneeId, Boolean assigned) {
        this.status = status;
        this.assigneeId = assigneeId;
        this.assigned = assigned;
    }

    @Override
    public Predicate toPredicate(Root<Task> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return Stream.of(getStatusPredicate(root, criteriaBuilder), getAssigneePredicate(root, criteriaBuilder))
                .filter(Objects::nonNull)
                .reduce(criteriaBuilder::and).orElse(null);
    }

    private Predicate getStatusPredicate(Root<Task> root, CriteriaBuilder criteriaBuilder) {
        return (status != null) ? criteriaBuilder.equal(root.get("status"), status) : null;
    }

    private Predicate getAssigneePredicate(Root<Task> root, CriteriaBuilder criteriaBuilder) {
        var joinedPersonTable = root.join("assignee", JoinType.LEFT);
        var personIdColumn = joinedPersonTable.get("id");
        if (assigneeId != null) {
            return criteriaBuilder.equal(personIdColumn, assigneeId);
        }
        if (assigned != null) {
            if (assigned) {
                return criteriaBuilder.isNotNull(personIdColumn);
            }
            return criteriaBuilder.isNull(personIdColumn);
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskSpecification that = (TaskSpecification) o;
        return status == that.status &&
                Objects.equals(assigneeId, that.assigneeId) &&
                Objects.equals(assigned, that.assigned);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, assigneeId, assigned);
    }
}
