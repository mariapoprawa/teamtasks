package com.example.teamtasks.service;

import com.example.teamtasks.dto.PersonDto;
import com.example.teamtasks.exception.NotEnoughCapacityLeftException;
import com.example.teamtasks.exception.ResourceNotFoundException;
import com.example.teamtasks.model.Person;
import com.example.teamtasks.model.Task;
import com.example.teamtasks.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person createNewPerson(PersonDto dto) {
        var person = convertToEntity(dto);
        return personRepository.save(person);
    }

    public Person getPerson(Long id) {
        return personRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Person with id " + id + " not found"));
    }

    public void deletePerson(Long id) {
        var person = getPerson(id);
        personRepository.delete(person);
    }

    public void assignTask(Person person, Task task) {
        var capacityLeft = calculateCapacityLeft(person);
        if (capacityLeft < task.getSize()) {
            throw new NotEnoughCapacityLeftException("Person with id " + person.getId() + " doesn't have capacity for this task");
        }

        person.addTask(task);
        personRepository.save(person);
    }

    public Optional<Person> findPersonWithCapacityLeft(int taskSize) {
        return personRepository.findAll().stream()
                .map(person -> Map.entry(person, calculateCapacityLeft(person)))
                .filter(e -> e.getValue() >= taskSize)
                .min(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey);
    }

    public int calculateCapacityLeft(Person person) {
        var tasksSize = person.getTasks().stream()
                .map(Task::getSize)
                .reduce(0, Integer::sum);
        return person.getCapacity() - tasksSize;
    }

    private Person convertToEntity(PersonDto dto) {
        return new Person(dto.getName(), dto.getCapacity());
    }
}
