package com.example.teamtasks.service;

import com.example.teamtasks.dto.TaskDto;
import com.example.teamtasks.exception.NotEnoughCapacityLeftException;
import com.example.teamtasks.exception.ResourceNotFoundException;
import com.example.teamtasks.model.Person;
import com.example.teamtasks.model.Status;
import com.example.teamtasks.model.Task;
import com.example.teamtasks.repository.TaskRepository;
import com.example.teamtasks.repository.specification.TaskSpecification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final PersonService personService;

    public TaskService(TaskRepository taskRepository, PersonService personService) {
        this.taskRepository = taskRepository;
        this.personService = personService;
    }

    public List<Task> getTasks(Status status, Long assigneeId, Boolean assigned) {
        var search = new TaskSpecification(status, assigneeId, assigned);
        return taskRepository.findAll(search);
    }

    public Task createTask(TaskDto dto) {
        var task = convertToEntity(dto);

        if (task.getAssignee() == null) {
            findAssigneeForTask(task.getSize()).ifPresent(task::setAssignee);
        }

        return taskRepository.save(task);
    }

    public Task getTask(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Task with id " + id + " not found"));
    }

    public void updateTask(Long id, TaskDto dto) {
        var existingTask = getTask(id);
        var oldTaskSize = existingTask.getSize();

        existingTask.setTitle(dto.getTitle());
        existingTask.setStatus(dto.getStatus());
        existingTask.setSize(dto.getSize());

        if (assigneeIsNotChanged(existingTask, dto)) {
            checkIfPersonHasEnoughCapacityLeft(existingTask.getAssignee(), oldTaskSize, dto.getSize());
        } else {
            assignTaskToPersonWithId(existingTask, dto.getAssigneeId());
        }

        taskRepository.save(existingTask);
    }

    public void deleteTask(Long id) {
        var task = getTask(id);
        taskRepository.delete(task);
    }

    public void reassignOpenTasks() {
        var openTasks = taskRepository.findAllByAssigneeIsNullOrderBySizeDesc();

        openTasks.forEach(task -> findAssigneeForTask(task.getSize()).ifPresent(person -> {
            task.setAssignee(person);
            personService.assignTask(person, task);
        }));
    }

    protected Optional<Person> findAssigneeForTask(int size) {
        return personService.findPersonWithCapacityLeft(size);
    }

    private Task convertToEntity(TaskDto dto) {
        var task = new Task(dto.getTitle(), dto.getStatus(), dto.getSize());
        assignTaskToPersonWithId(task, dto.getAssigneeId());
        return task;
    }

    private void assignTaskToPersonWithId(Task task, Long personId) {
        if (personId != null) {
            var assignee = personService.getPerson(personId);
            task.setAssignee(assignee);
            personService.assignTask(assignee, task);
        }
    }

    private boolean assigneeIsNotChanged(Task existingTask, TaskDto dto) {
        return existingTask.getAssignee() != null && existingTask.getAssignee().getId().equals(dto.getAssigneeId());
    }

    private void checkIfPersonHasEnoughCapacityLeft(Person assignee, int oldTaskSize, int newSize) {
        var difference = newSize - oldTaskSize;
        var capacityLeft = personService.calculateCapacityLeft(assignee);

        if (difference > capacityLeft) {
            throw new NotEnoughCapacityLeftException("Person with id " + assignee.getId() + " doesn't have capacity for this task");
        }
    }
}
