package com.example.teamtasks.model;

import com.example.teamtasks.validator.FibonacciNumber;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Task {

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String title;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @FibonacciNumber
    private int size;

    @ManyToOne
    private Person assignee;

    public Task() {

    }

    public Task(String title, Status status, int size) {
        this.title = title;
        this.status = status;
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Person getAssignee() {
        return assignee;
    }

    public void setAssignee(Person assignee) {
        this.assignee = assignee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return size == task.size &&
                Objects.equals(id, task.id) &&
                Objects.equals(title, task.title) &&
                status == task.status &&
                Objects.equals(assignee, task.assignee);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, status, size, assignee);
    }
}
