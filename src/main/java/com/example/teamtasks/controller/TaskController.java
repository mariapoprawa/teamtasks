package com.example.teamtasks.controller;

import com.example.teamtasks.dto.TaskDto;
import com.example.teamtasks.model.Status;
import com.example.teamtasks.model.Task;
import com.example.teamtasks.service.TaskService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTasks(@RequestParam(required = false) Status status,
                                      @RequestParam(required = false) Long assignee,
                                      @RequestParam(required = false) Boolean assigned) {
        var list = taskService.getTasks(status, assignee, assigned);
        return ResponseEntity.ok(list);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createTask(@Valid @RequestBody TaskDto task) {
        var createdTask = taskService.createTask(task);
        return ResponseEntity.ok(createdTask);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTask(@PathVariable Long id) {
        var task = taskService.getTask(id);
        return ResponseEntity.ok(task);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateTask(@PathVariable Long id, @Valid @RequestBody TaskDto task) {
        taskService.updateTask(id, task);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable Long id) {
        taskService.deleteTask(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/reassign")
    public ResponseEntity<?> reassignOpenTasks() {
        taskService.reassignOpenTasks();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
