package com.example.teamtasks.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FibonacciNumberValidator implements ConstraintValidator<FibonacciNumber, Integer> {
    @Override
    public void initialize(FibonacciNumber constraintAnnotation) {

    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return integer != null && (integer == 0 || isFibonacciNumber(integer, 0, 1));
    }

    private boolean isFibonacciNumber(int number, int lastButOneTerm, int lastTerm) {
        var nextTerm = lastButOneTerm + lastTerm;
        return number == nextTerm || (number > nextTerm  && isFibonacciNumber(number, lastTerm, nextTerm));
    }
}
