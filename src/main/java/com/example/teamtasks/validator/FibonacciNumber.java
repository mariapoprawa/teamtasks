package com.example.teamtasks.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = FibonacciNumberValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface FibonacciNumber {
    String message() default "Not a Fibonacci number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
