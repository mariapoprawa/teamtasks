package com.example.teamtasks.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NotEnoughCapacityLeftException extends RuntimeException {

    public NotEnoughCapacityLeftException(String message) {
        super(message);
    }

    public NotEnoughCapacityLeftException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotEnoughCapacityLeftException(Throwable cause) {
        super(cause);
    }

    public NotEnoughCapacityLeftException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
